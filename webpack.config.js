var path = require("path");
var webpack = require('webpack');
module.exports = {
    entry: [
        // Set up an ES6-ish environment
        //'babel-polyfill',
        "./lib/Entry.js"
    ],
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "sqlitejs.js",
        publicPath: "dist",
        //sourceMapFilename: "bundle.js.map",
        libraryTarget: "umd",
        library: "sqlitejs"
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin(
            '[file].map', null,
            "[absolute-resource-path]", "[absolute-resource-path]"),
        //new webpack.optimize.UglifyJsPlugin(),
        new webpack.DefinePlugin({
            'NODE_ENV': JSON.stringify('production')
        })
    ],
    //resolveLoader: {
    //    root: path.join(__dirname, 'node_modules')
    //},
    module: {
        loaders: [
            {
                test: /\.js$/,

                loader: "babel-loader",

                exclude: [
                    path.resolve(__dirname, "node_modules"),
                    path.resolve(__dirname, "vendor")
                ],

                //// Skip any files outside of your project's `src` directory
                //include: [
                //    path.resolve(__dirname, "src"),
                //],
                // Options to configure babel with
                query: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015']
                }
            }
        ]
    },

    externals: {
        "lodash": "lodash"
    }

};