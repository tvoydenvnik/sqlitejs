(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['sqlitejs'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        factory(require('../lib/Entry'));
    } else {
        // Browser globals (root is window)
        factory(root.sqlitejs);
    }
}(this, function (Sqlitejs) {


    var SqliteWrapper = Sqlitejs.SqliteWrapper;
    var Account = Sqlitejs.Account;


    SqliteWrapper.setDefaultDB({
        name: 'jasmine_test_account',
        version: '1',
        displayname: 'jasmine_test_account',
        size: 1000000,
        //sqlitePlugin: false,
        debug: true
    });


    describe('Account  tests', function(done) {

        beforeEach(function(done) {
            done();
        }, 15000);

        afterEach(function(done) {
            done();
        }, 15000);


        it('drop table',function(done){

            Account.dropTable('jasmine_test_account').then(function() {
                expect(1).toBe(1);
                done()
            });

        });

        //it('Create table', function(done) {
        //
        //    Account.createTableIfNotExist().then(function(bCreate){
        //        expect(bCreate).toBe(true);
        //        done();
        //    });
        //
        //});
        //
        //
        //it('Create table - таблица уже созданна', function(done) {
        //    var account = new Account();
        //
        //    account.createTableIfNotExist().then(function(bCreate){
        //        expect(bCreate).toBe(false);
        //        done();
        //    });
        //
        //});


        it('isEmpty = true', function(done) {

            Account.isEmpty().then(function(isEmpty){
                expect(isEmpty).toBe(true);

                done();
            });

        });

        it('save', function(done) {
            var account = new Account();
            account.name = 'test';

            account.save().then(function(){
                expect(account.id).toBe(1);

                done();
            });

        });


        it('isEmpty = false', function(done) {

            Account.isEmpty().then(function(isEmpty){
                expect(isEmpty).toBe(false);

                done();
            });

        });

        it('byId - найдем ранее созданный id = 1', function(done) {
            var account = new Account();

            account.byId(1).then(function(bFind){
                expect(bFind).toBe(true);
                expect(account.id).toBe(1);
                done();
            });

        });


        it('byId (static) - найдем ранее созданный id = 1', function(done) {


            Account.byId(1).then(function(account){
                expect(account.isFind).toBe(true);
                expect(account.id).toBe(1);
                done();
            });

        });

        it('byId - такого не существует id = 1111', function(done) {
            var account = new Account();

            account.byId(1111).then(function(bFind){
                expect(bFind).toBe(false);

                done();
            });

        });


    });


}));
