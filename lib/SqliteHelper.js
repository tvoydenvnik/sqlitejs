var _ = require('lodash');
var SqliteHelper = function(config) {

    if (window && window.sqlitePlugin) {
        //cordova or phonegap
        this._db = window.sqlitePlugin.openDatabase(config.name, config.version, config.displayname, config.size);
    } else {
        this._db = openDatabase(config.name, config.version, config.displayname, config.size);
    }
    this.debug = config.debug || false;

    this._cache = {};

};

SqliteHelper.prototype._toWebSQLValue = function(val) {

    if (_.isObject(val) || _.isArray(val)) {
        if (val instanceof(Date)) {
            val = val.getTime();
        } else {
            val = JSON.stringify(val);
        }
    }
    if(_.isString(val)) {
        val = "'"+val+"'";
    }
    return val;
};

SqliteHelper.prototype._fromWebSqlToJsValue = function(table, result) {
    var rows = [];
    if (result && result.rows && result.rows.length!==0) {
        for(var i = 0; i < result.rows.length; i++) {
            var item = result.rows.item(i);

            var lResult = {};

            for (var key in item) {
                if (item.hasOwnProperty(key)) {

                    lResult[key] = item[key];
                    var lTypeOfColumn = this._cache[table+'_'+key];
                    if (lTypeOfColumn==='json') {
                        lResult[key] = JSON.parse(item[key]);
                    } else if (lTypeOfColumn==='date' || key==='created_at' || key==='updated_at') {
                        lResult[key] = new Date(item[key]);
                    }else if(lResult[key] === null || lResult[key]===undefined){
                        if (lTypeOfColumn==='text') {
                            lResult[key] = '';
                        }else if(lTypeOfColumn==='numeric' || lTypeOfColumn==='integer') {
                            lResult[key] = 0;
                        }
                    }
                }
            }

            rows.push(lResult);
        }
        return rows;
    } else {
        return null;
    }

};

SqliteHelper.prototype.queryOld = function(sql, callback) {
    var debug = this.debug;

    this._db.transaction(function(tx) {
        if (debug){
            console.log('qeury: '+sql);
        }

        tx.executeSql(sql, [], function(tx, result) {

            if (_.isFunction(callback)){
                callback(tx, result);
            }

        }, function(tx, error) {

            console.error('WebSQL', sql, error.code, error.message);

            if (_.isFunction(callback)){

                callback(tx, null, error);
            }
        });
    });
};

SqliteHelper.prototype.query = function(sql, alwaysResolve = false) {
    var debug = this.debug;

    var self = this;
    return new Promise(function(resolve, reject){
        self._db.transaction(function(tx) {
            if (debug){
                console.log('query: '+sql);
            }

            tx.executeSql(sql, [], function(tx, result) {

                resolve(result, tx);

            }, function(tx, error) {

                console.error('WebSQL', sql, error.code, error.message);

                if(alwaysResolve === true){
                    resolve(error, tx);
                }else{
                    reject(error, tx);
                }


            });
        });
    });


};

/**
 * Формирует запрос создания таблицы
 * @param oStructure
 * @returns {string}
 * @private
 */

SqliteHelper.prototype._createTableSql= function(oStructure){
    var self =this,
        query = 'CREATE TABLE '+oStructure.name+' (',
        fields = ['id INTEGER PRIMARY KEY AUTOINCREMENT', 'created_at REAL', 'updated_at REAL'];


    _.forOwn(oStructure.fields, function(sType, field){

        self._cache[oStructure.name+'_'+field] = sType;
        if (sType==='json'){
            sType = 'text';
        }else if (sType==='date') {
            sType = 'real';
        }
        fields.push(field+' '+sType.toUpperCase());

    });

    query += (fields.join(', ')) + ')';

    return query;
};
/*
 * Создание таблицы. Данная функция должна быть обязательно вызвана, перед работой с данной таблицей.
 */
SqliteHelper.prototype.createTable = function(oStructure, bReCreate) {

    var self = this;
    return new Promise(function(resolve, reject){

        self.createTableOld(oStructure,function(tx, result, error, command){

            if (error){
                reject(error);
            }else{
                resolve(command, result);
            }
        }, bReCreate)


    });
};
SqliteHelper.prototype.createTableOld = function(oStructure, callback, bReCreate) {
    var self = this;
    var query = this._createTableSql(oStructure);


    self.queryOld(this._createTableSql(oStructure), function(tx, result, error) {

        // if table already exists
        if (error && error.code===5) {

            self.queryOld('SELECT * FROM '+oStructure.name+' LIMIT 1', function(tx, res, error) {
                if (!res || res.rows.length===0 && bReCreate!==true){

                    //таблица существует, но записей не имеет - удалим и пересоздаим таблицу.
                    self.dropTable(oStructure.name).then(function(){

                        self.createTableOld(oStructure, callback, true);

                    });

                }else{
                    var rows = _.keys(res.rows.item(0)),
                        newRows = _.keys(oStructure.fields),
                        q = 'ALTER TABLE '+oStructure.name+' ADD COLUMN ';

                    var arQueries = [];
                    var sColumns = '';
                    newRows.map(function(row) {
                        if (rows.indexOf(row)===-1){
                            arQueries.push(q + row + ' '+oStructure.fields[row]);
                            sColumns = sColumns +  row+';';
                        }

                    });

                    var lTotal = 0;
                    if(arQueries.length === 0){
                        if(_.isFunction(callback)){
                            callback(tx, result, error);
                        }
                    }else{
                        arQueries.forEach(function(sQuery){
                            self.queryOld(sQuery, function(tx, result, error){
                                lTotal = lTotal + 1;

                                if(lTotal === arQueries.length && _.isFunction(callback)){
                                    callback(tx, result, error, 'add columns:'+sColumns);
                                }

                            });

                        });

                    }


                }

            });
        }else{
            if (_.isFunction(callback)) callback(tx, result, error, (bReCreate!==true?'created':'drop created'));
        }


        oStructure.index.map(function(index) {
            var keys = _.keys(index),
                indexes = [],
                q = index.fields.join('')+' ON '+oStructure.name+' ('+index.fields.join(', ')+')';
            keys.map(function(key) {
                if (key!=='fields')
                    indexes.push(key);
            });
            q = 'CREATE '+indexes.join(' ').toUpperCase()+' INDEX IF NOT EXISTS '+q;
            self.queryOld(q);
        });

    });

    return query;

};

SqliteHelper.prototype._queryAll = function(table) {
    return 'SELECT * FROM '+table;
};
SqliteHelper.prototype.all = function(table, callback) {
    var self = this,
        query = this._queryAll(table);

    this.queryOld(query, function(tx, result) {
        var rows = self._fromWebSqlToJsValue(table, result);
        if (_.isFunction(callback)) callback(rows);
    });

    return query;
};


SqliteHelper.prototype._queryById = function(table, id) {
    return 'SELECT * FROM '+table+' WHERE id='+id;
};
SqliteHelper.prototype.byId = function(table, id, callback) {

    var query = this._queryById(table, id);
    var self = this;

    return self.query(query).then(function(res){
        var result = self._fromWebSqlToJsValue(table, res);
        result = result ? result[0] : null;
        return result;

    });

};
SqliteHelper.prototype._queryFind = function(table, cond) {
    var query = 'SELECT ',
        self = this;

    if (cond.select) {
        if (_.isString(cond.select)) {

            query += cond.select;

        }else if( _.isArray(cond.select)){

            query += cond.select.join(', ');

        }else{
            query += '*';
        }

    } else {

        query += '*';

    }

    query += ' FROM '+table;

    if (cond.where) {
        query += ' WHERE';
        cond.where.map(function(where) {
            if (_.isObject(where)) {
                var value = _.isString(where.value) ? "'"+where.value+"'" : where.value;
                query += ' '+where.field + where.op + value;
            } else {
                query += ' '+where;
            }
        });
    }

    if (cond.order) {
        query += ' ORDER BY '+cond.order;
    }

    if (cond.limit) {
        query += ' LIMIT '+cond.limit;
    }

    return query;
};


SqliteHelper.prototype.find =  function(table, cond) {
    var query = this._queryFind(table, cond),
        self = this;

    return this.query(query).then(
        function(res) {
            var result = [];
            if (cond.row!==true)
                result = self._fromWebSqlToJsValue(table, res);
            else {
                if (res && res.rows.length!==0) {
                    for(var i = 0; i < res.rows.length; i++) {
                        result.push(res.rows.item(i));
                    }
                }
            }

            return result;
        }
    );


};

SqliteHelper.prototype._queryInsert = function(table, data) {
    //if (!_.isArray(data)) data = [data];
    var defaultKeys = _.keys(data),
        keys = [];

    defaultKeys.map(function(key) {
        if (data[key] || data[key]==="" || data[key]===0){
            keys.push(key);
        } else{
            //console.log(data[key],key );
        }
    });

    keys = ['created_at', 'updated_at'].concat(keys);

    var created = new Date().getTime(),
        vals = [ created, created ],
        self = this;

    keys.map(function(key) {
        if (data[key] || data[key]==="" || data[key]===0){
            vals.push(self._toWebSQLValue(data[key]));
        }


    });

    return 'INSERT INTO '+table+' ('+keys.join(', ')+') VALUES ('+vals.join(', ')+')';
};

SqliteHelper.prototype.insert= function(table, data) {

    if(data.id === 0){
        data.id = null;
    }
    var query = this._queryInsert(table, data);

    return this.query(query).then(function(res) {
        var id = res ? res.insertId : null;
        return id;
    });
};

SqliteHelper.prototype._queryUpdate= function(table, data) {
    var self = this,
        set = ['updated_at='+(new Date().getTime())],
        where = '';

    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            if (key==='id') {
                where = 'id='+data[key];
            } else {
                set.push(key+'='+self._toWebSQLValue(data[key]));
            }
        }
    }

    return 'UPDATE '+table+' SET '+set.join(', ')+' WHERE '+where;
};
SqliteHelper.prototype.update = function(table, data) {
    if (!data.id) return;
    var query = this._queryUpdate(table, data);
    return this.query(query);

};

SqliteHelper.prototype.save = function(table, data) {
    var id = data.id || null,
        self = this;

    if (id) {
        return this.byId(table, id).then(function(result) {
            if (!result) {
                return self.insert(table, data);
            } else {
                return self.update(table, data);
            }
        });
    } else {
        return self.insert(table, data);
    }
};

SqliteHelper.prototype.dropTable = function(table) {
    var query = 'DROP TABLE '+table;
    return this.query(query);
};

SqliteHelper.prototype.deleteById = function(table, id) {
    var query = 'DELETE FROM '+table+' WHERE id='+id;
    this.queryOld(query, function(tx, res, error) {

    });
};

module.exports = SqliteHelper;