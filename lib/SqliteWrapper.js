var SqliteHelper = require('./SqliteHelper');

var _defaultDb = null;
var _createTableIfNotExistCache = {};
var _dbCache = {};

var _ = require('lodash');

var  SqliteWrapper = function(oSchema, oClass){

    oClass.prototype.tableSchema = function(){
        return oSchema;
    };

    oClass._schema = oSchema;


    //oClass.prototype.createTableIfNotExist = function(bReCreate = false){
    //    return SqliteWrapper.createTableIfNotExist(oClass._schema, bReCreate = false);
    //};


    oClass.prototype.save = function(){
        var self = this;
        let oSchema = this.tableSchema();
        return SqliteWrapper.createTableIfNotExist(oSchema).then(
            function(){

                let data = SqliteWrapper.toObject(oSchema, self);
                return SqliteWrapper.getDefaultDB().save(oSchema.name, data).then(function(nId){

                    self.id = nId;
                });

            }
        );

    };

    oClass.prototype.byId = function(nId){
        var self = this;
        let oSchema = this.tableSchema();
        return SqliteWrapper.createTableIfNotExist(oSchema).then(function(){
            return SqliteWrapper.getDefaultDB().byId(oSchema.name, nId).then(function(arResult){

                if(arResult === null){
                    return false;
                }else{
                    SqliteWrapper.fromObject(oSchema, self, arResult);
                    return true;
                }



            });

        });
    };

    oClass.byId = function(nId){

        var newInstance = new oClass();
        return newInstance.byId(nId).then(function(bIsFind){
            newInstance.isFind = bIsFind;
            return newInstance;
        });

    };

    oClass.byDateAndIdUser = function(){

    };

    oClass.byPeriodAndIdUser = function(){

    };

    oClass.dropTable = function(){
        //var oSchema = this.tableSchema();
        return SqliteWrapper.getDefaultDB().dropTable(oSchema['name']);
    };

    oClass.isEmpty = function(){


        return SqliteWrapper.createTableIfNotExist(oSchema).then(
            function(){

                return SqliteWrapper.getDefaultDB().query("SELECT count(id) AS ct FROM " + oSchema.name).then(
                    function(res) {

                        return ((res && res.rows.length>0 && res.rows.item(0).ct>0 )?false:true);

                    }
                );
            }
        );

    }



};


SqliteWrapper.createTableIfNotExist = function(oSchema, bReCreate = false){

    return new Promise(function(resolve, reject){

        if(_createTableIfNotExistCache[oSchema['name']] === true && bReCreate!==true){
            resolve(false);
        }else{
            SqliteWrapper.getDefaultDB().createTable(oSchema).then(function(){
                _createTableIfNotExistCache[oSchema['name']] = true;
                resolve(true);
            }, function(){
                reject();
            });
        }

    });


};

SqliteWrapper.fromObject = function(oSchema, oInstanceOfClass, oDataFromDB){


    if(oDataFromDB===null){
        return oInstanceOfClass;
    }

    _(oSchema.fields)
        .forOwn(function(value, key){
                oInstanceOfClass[key] = oDataFromDB[key];
            }
        ).value();

    oInstanceOfClass.id = oDataFromDB.id;
    oInstanceOfClass.created_at = oDataFromDB.created_at;
    oInstanceOfClass.updated_at = oDataFromDB.updated_at;

    return oInstanceOfClass;
};

SqliteWrapper.toObject = function(oSchema, oInstanceOfClass){

    var lResult = {};
    _(oSchema.fields)
        .forOwn(function(value, key){
                lResult[key] = oInstanceOfClass[key];
            }
        ).value();
    lResult.id = oInstanceOfClass.id;

    return lResult;
};

SqliteWrapper._openDatabase = function(config) {
    if (!_dbCache[config.name]) {
        _dbCache[config.name] = new SqliteHelper(config);
    }
    return _dbCache[config.name];
};

/*
 Установим бд по умолчанию
 */
SqliteWrapper.setDefaultDB = function(config) {
    _defaultDb = SqliteWrapper._openDatabase(config);
    return _defaultDb;
};

SqliteWrapper.getDefaultDB = function() {
    return _defaultDb;
};

module.exports = SqliteWrapper;