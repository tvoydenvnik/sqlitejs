var SqliteHelper = require('./SqliteHelper');

var _defaultDb = null;
var _createTableIfNotExistCache = {};
var _dbCache = {};

var _ = require('lodash');

class SqliteWrapper {
    constructor() {

    }


    createTableIfNotExist(bReCreate = false){

        var self = this;

        return new Promise(function(resolve, reject){

            var oSchema = self.tableSchema();
            if(_createTableIfNotExistCache[oSchema['name']] === true && bReCreate!==true){
                resolve(false);
            }else{
                self.getDB().createTable(oSchema).then(function(){
                    _createTableIfNotExistCache[oSchema['name']] = true;
                    resolve(true);
                }, function(){
                    reject();
                });
            }




        });



    }

    dropTable(){
        var oSchema = this.tableSchema();
        return this.getDB().dropTable(oSchema['name']);
    }

    static fromObject(oSchema, oInstanceOfClass, oDataFromDB){


        if(oDataFromDB===null){
            return oInstanceOfClass;
        }

        _(oSchema.fields)
            .forOwn(function(value, key){
                    oInstanceOfClass[key] = oDataFromDB[key];
                }
            ).value();

        oInstanceOfClass.id = oDataFromDB.id;
        oInstanceOfClass.created_at = oDataFromDB.created_at;
        oInstanceOfClass.updated_at = oDataFromDB.updated_at;

        return oInstanceOfClass;
    }

    static toObject(oSchema, oInstanceOfClass){

        var lResult = {};
        _(oSchema.fields)
            .forOwn(function(value, key){
                    lResult[key] = oInstanceOfClass[key];
                }
            ).value();
        lResult.id = oInstanceOfClass.id;

        return lResult;
    }

    save(){

        var self = this;
        let oSchema = this.tableSchema();
        return self.createTableIfNotExist().then(
            function(){

                let data = SqliteWrapper.toObject(oSchema, self);
                return self.getDB().save(oSchema.name, data).then(function(nId){

                    self.id = nId;
                });

            }
        );


    }


    byId(nId){

        var self = this;
        let oSchema = this.tableSchema();
        return self.createTableIfNotExist(oSchema).then(function(){
            return self.getDB().byId(oSchema.name, nId).then(function(arResult){

                if(arResult === null){
                    return false;
                }else{
                    SqliteWrapper.fromObject(oSchema, self, arResult);
                    return true;
                }



            });

        });

    }


    isEmpty(){

        var self = this;
        let oSchema = this.tableSchema();
        return self.createTableIfNotExist(oSchema).then(
            function(){

                return self.getDB().query("SELECT count(id) AS ct FROM " + oSchema.name).then(
                    function(res) {

                        return ((res && res.rows.length>0 && res.rows.item(0).ct>0 )?false:true);

                    }
                );
            }
        );

    }

    /**
     * @returns {SqliteWrapper}
     */
    getDB(){
        return SqliteWrapper.getDefaultDB();
    }

    static _openDatabase(config) {
        if (!_dbCache[config.name]) {
            _dbCache[config.name] = new SqliteHelper(config);
        }

        return _dbCache[config.name];
    }

    /*
     Установим бд по умолчанию
     */
    static setDefaultDB(config) {
        _defaultDb = SqliteWrapper._openDatabase(config);
        return _defaultDb;
    }

    static getDefaultDB() {
        return _defaultDb;
    }


}

module.exports = SqliteWrapper;